(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("underscore"), require("backbone-relation"));
	else if(typeof define === 'function' && define.amd)
		define(["underscore", "backbone-relation`"], factory);
	else if(typeof exports === 'object')
		exports["backbone-relation-repository"] = factory(require("underscore"), require("backbone-relation"));
	else
		root["backbone-relation-repository"] = factory(root["_"], root["backbone-relation"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_1__, __WEBPACK_EXTERNAL_MODULE_2__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; }; /**
	                                                                                                                                                                                                                                                   * Override the constructor to perhaps create the relations if `createRelations`
	                                                                                                                                                                                                                                                   * is set to true.
	                                                                                                                                                                                                                                                   *
	                                                                                                                                                                                                                                                   * @param {[type]} key     [description]
	                                                                                                                                                                                                                                                   * @param {[type]} val     [description]
	                                                                                                                                                                                                                                                   * @param {[type]} options [description]
	                                                                                                                                                                                                                                                   */

	// The unit tests all use Backbone.Model from the global namespace. Here we
	// extend the Backbone.Model, but eventually replace Backbone.Model with the
	// patched one and run unit test against it.

	var _underscore = __webpack_require__(1);

	var _underscore2 = _interopRequireDefault(_underscore);

	var _backboneRelation = __webpack_require__(2);

	var _backboneRelation2 = _interopRequireDefault(_backboneRelation);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	module.exports = _backboneRelation2.default.extend({
	    repository: null,
	    main: null,
	    constructor: function constructor(attrs) {
	        var options = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];

	        this.repository = options.repository;
	        this.main = options.main;

	        return _backboneRelation2.default.prototype.constructor.call(this, attrs, options);
	    },
	    convertAttributes: function convertAttributes(key, val, options) {
	        var attrs = {};

	        if (key === null) return this;

	        // Handle both `"key", value` and `{key: value}` -style arguments.
	        if ((typeof key === 'undefined' ? 'undefined' : _typeof(key)) === 'object') {
	            attrs = key;
	            options = val;
	        } else if (val === undefined && (options === undefined || options.unset !== true)) {
	            attrs = this.formatScalar(key, options);
	        } else {
	            attrs[key] = val;
	        }

	        return {
	            attrs: attrs,
	            options: options
	        };
	    },
	    set: function set(key, val, options) {
	        var converted = this.convertAttributes(key, val, options);

	        return _backboneRelation2.default.prototype.set.call(this, converted.attrs, converted.options);
	    },
	    formatScalar: function formatScalar(val, options) {
	        if (typeof val === 'number') {
	            return this.formatScalarNumber(val, options);
	        } else if (val instanceof Backbone.Model) {
	            return this.formatScalarModel(val, options);
	        }
	    },
	    formatScalarNumber: function formatScalarNumber(val) {
	        return { id: val };
	    },
	    formatScalarModel: function formatScalarModel(val) {
	        return _underscore2.default.clone(val.attributes);
	    },

	    /**
	     * Create an instance of the relation. It differs from backbone-relation by
	     * first trying to find the correct Model in the given repository. If it
	     * cannot be found, default to the parent's createRelated.
	     */
	    createRelated: function createRelated(relation, val, constructor, options) {
	        var hasRepository = options && options.repository && options.repository.get(relation);

	        if (hasRepository) {
	            var ids = val;
	            if (Array.isArray(ids) && ids.length > 0 && _underscore2.default.every(ids, function (id) {
	                return typeof id === 'number';
	            })) {
	                val = ids.map(function (id) {
	                    if (options.repository.get(relation).get('collection').get(id)) {
	                        return _underscore2.default.clone(options.repository.get(relation).get('collection').get(id));
	                    }

	                    return { id: id };
	                });
	            } else {
	                val = _underscore2.default.clone(options.repository.get(relation).get('collection').get(val));
	            }
	        }

	        return _backboneRelation2.default.prototype.createRelated.call(this, relation, val, constructor, options);
	    },

	    /**
	    * If an id is give, find that model in the repository first.
	    *
	    * @param {Backbone.Model} model
	    * @param {mixed} value
	    * @param {mixed} options
	    */
	    setByModel: function setByModel(model, value, options) {
	        if (this.isIdValue(value, options)) {
	            var maybeModel = this.fetchFromRepository(this.isIdValue(value, options), options);

	            if (maybeModel) {
	                // Join attributes.
	                if (value !== this.isIdValue(value, options)) {
	                    value = _underscore2.default.extend({}, maybeModel, value);
	                } else {
	                    value = maybeModel;
	                }
	            }
	        }

	        return _backboneRelation2.default.prototype.setByModel.call(this, model, value, options);
	    },

	    /**
	     * @param {Backbone.Collection} collection
	     * @param {mixed} value
	     * @param {mixed} options
	     */
	    setByCollection: function setByCollection(collection, value, options) {
	        var _this = this;

	        var converted = undefined;

	        if (Array.isArray(value)) {
	            converted = value.map(function (maybeId) {
	                if (_this.isIdValue(maybeId, options)) {
	                    var maybeModel = _this.fetchFromRepository(_this.isIdValue(maybeId, options), options);

	                    if (maybeModel) {
	                        // Join attributes.
	                        if (maybeId !== _this.isIdValue(maybeId, options)) {
	                            return _underscore2.default.extend({}, maybeModel, maybeId);
	                        }

	                        return maybeModel;
	                    }

	                    // Join attributes.
	                    if (maybeId !== _this.isIdValue(maybeId, options)) {
	                        return maybeId;
	                    }

	                    return { id: maybeId };
	                }

	                return maybeId;
	            });
	        } else {
	            converted = value;
	        }

	        return _backboneRelation2.default.prototype.setByCollection.call(this, collection, converted, options);
	    },
	    isIdValue: function isIdValue(value, options) {
	        var relations = _underscore2.default.result(this, 'relations');

	        if (value === undefined || value === null || typeof value === 'string' || typeof value === 'number') {
	            return value;
	        } else if (relations[options.relation].join_attributes && value.id) {
	            return value.id;
	        }

	        return undefined;
	    },
	    fetchFromRepository: function fetchFromRepository(id, options) {
	        if (this.repository && this.repository.get(options.relation) && this.repository.get(options.relation).get('collection').get(id)) {
	            options.repository = this.repository;
	            return this.repository.get(options.relation).get('collection').get(id);
	        } else if (options.repository && options.repository.get(options.relation) && options.repository.get(options.relation).get('collection').get(id)) {
	            return options.repository.get(options.relation).get('collection').get(id);
	        }

	        return undefined;
	    }
	});

	// Backbone.Model = BurhanModel2;

	// const BC2 = Backbone.Collection;
	// export const Collection = Backbone.Collection.extend({
	//     set(models, options) {
	//         // Check if models is an array of ids.
	//         if (Array.isArray(models) && models.length > 0 && _.every(models, m => typeof m === 'number')) {
	//             models = models.map(id => {
	//                 // const relation = 'writers';
	//                 // const val = id;

	//                 // if (options && options.repository && options.repository.get(relation) && options.repository.get(relation).get('collection').get(val)) {
	//                 //     return options.repository.get(relation).get('collection').get(val).clone().attributes;
	//                 // }

	//                 return { id };
	//             });
	//         }

	//         return Backbone.Collection.prototype.set.call(this, models, options);
	//     },
	// });

	// Backbone.Collection = BurhanCollection2;
	// ---- TEMP HACK to run unit tests ----

/***/ },
/* 1 */
/***/ function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_1__;

/***/ },
/* 2 */
/***/ function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_2__;

/***/ }
/******/ ])
});
;