/**
 * Override the constructor to perhaps create the relations if `createRelations`
 * is set to true.
 *
 * @param {[type]} key     [description]
 * @param {[type]} val     [description]
 * @param {[type]} options [description]
 */

// The unit tests all use Backbone.Model from the global namespace. Here we
// extend the Backbone.Model, but eventually replace Backbone.Model with the
// patched one and run unit test against it.

import _ from 'underscore';
import BM from 'backbone-relation';

module.exports = BM.extend({
    repository: null,
    main: null,
    constructor(attrs, options = {}) {
        this.repository = options.repository;
        this.main = options.main;

        return BM.prototype.constructor.call(this, attrs, options);
    },
    convertAttributes(key, val, options) {
        let attrs = {};

        if (key === null) return this;

        // Handle both `"key", value` and `{key: value}` -style arguments.
        if (typeof key === 'object') {
            attrs = key;
            options = val;
        } else if (val === undefined && (options === undefined || options.unset !== true)) {
            attrs = this.formatScalar(key, options);
        } else {
            attrs[key] = val;
        }

        return {
            attrs,
            options,
        };
    },
    set(key, val, options) {
        const converted = this.convertAttributes(key, val, options);

        return BM.prototype.set.call(this, converted.attrs, converted.options);
    },
    formatScalar(val, options) {
        if (typeof val === 'number') {
            return this.formatScalarNumber(val, options);
        } else if (val instanceof Backbone.Model) {
            return this.formatScalarModel(val, options);
        }
        return val;
    },
    formatScalarNumber(val) {
        return { id: val };
    },
    formatScalarModel(val) {
        return _.clone(val.attributes);
    },
    /**
     * Create an instance of the relation. It differs from backbone-relation by
     * first trying to find the correct Model in the given repository. If it
     * cannot be found, default to the parent's createRelated.
     */
    createRelated(relation, val, constructor, options) {
        const hasRepository = options && options.repository && options.repository.get(relation);

        if (hasRepository) {
            const ids = val;
            if (Array.isArray(ids) && ids.length > 0 && _.every(ids, id => typeof id === 'number')) {
                val = ids.map((id) => {
                    if (options.repository.get(relation).get('collection').get(id)) {
                        return _.clone(options.repository.get(relation).get('collection').get(id));
                    }

                    return { id };
                });
            } else {
                val = _.clone(options.repository.get(relation).get('collection').get(val));
            }
        }

        return BM.prototype.createRelated.call(this, relation, val, constructor, options);
    },
     /**
     * If an id is give, find that model in the repository first.
     *
     * @param {Backbone.Model} model
     * @param {mixed} value
     * @param {mixed} options
     */
    setByModel(model, value, options) {
        if (this.isIdValue(value, options)) {
            const maybeModel = this.fetchFromRepository(this.isIdValue(value, options), options);

            if (maybeModel) {
                // Join attributes.
                if (value !== this.isIdValue(value, options)) {
                    value = _.extend({}, maybeModel, value);
                } else {
                    value = maybeModel;
                }
            }
        }

        return BM.prototype.setByModel.call(this, model, value, options);
    },
    /**
     * @param {Backbone.Collection} collection
     * @param {mixed} value
     * @param {mixed} options
     */
    setByCollection(collection, value, options) {
        let converted;

        if (Array.isArray(value)) {
            converted = value.map((maybeId) => {
                if (this.isIdValue(maybeId, options)) {
                    const maybeModel = this.fetchFromRepository(this.isIdValue(maybeId, options), options);

                    if (maybeModel) {
                        // Join attributes.
                        if (maybeId !== this.isIdValue(maybeId, options)) {
                            return _.extend({}, maybeModel, maybeId);
                        }

                        return maybeModel;
                    }

                    // Join attributes.
                    if (maybeId !== this.isIdValue(maybeId, options)) {
                        return maybeId;
                    }

                    return { id: maybeId };
                }

                return maybeId;
            });
        } else {
            converted = value;
        }

        return BM.prototype.setByCollection.call(this, collection, converted, options);
    },
    isIdValue(value, options) {
        const relations = _.result(this, 'relations');

        if (value === undefined || value === null || typeof value === 'string' || typeof value === 'number') {
            return value;
        } else if (relations[options.relation].join_attributes && value.id) {
            return value.id;
        }

        return undefined;
    },
    fetchFromRepository(id, options) {
        if (
            this.repository &&
            this.repository.get(options.relation) &&
            this.repository.get(options.relation).get('collection').get(id)
        ) {
            options.repository = this.repository;
            return this.repository.get(options.relation).get('collection').get(id);
        } else if (
            options.repository &&
            options.repository.get(options.relation) &&
            options.repository.get(options.relation).get('collection').get(id)
        ) {
            return options.repository.get(options.relation).get('collection').get(id);
        }

        return undefined;
    },
});

// Backbone.Model = BurhanModel2;

// const BC2 = Backbone.Collection;
// export const Collection = Backbone.Collection.extend({
//     set(models, options) {
//         // Check if models is an array of ids.
//         if (Array.isArray(models) && models.length > 0 && _.every(models, m => typeof m === 'number')) {
//             models = models.map(id => {
//                 // const relation = 'writers';
//                 // const val = id;

//                 // if (options && options.repository && options.repository.get(relation) && options.repository.get(relation).get('collection').get(val)) {
//                 //     return options.repository.get(relation).get('collection').get(val).clone().attributes;
//                 // }

//                 return { id };
//             });
//         }

//         return Backbone.Collection.prototype.set.call(this, models, options);
//     },
// });

// Backbone.Collection = BurhanCollection2;
// ---- TEMP HACK to run unit tests ----
