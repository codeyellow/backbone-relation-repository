- Fix tests by upgrading stack to match backbone-relation.


# backbone-relation-repository

Extends backbone-relation with a repository. If a repository is given to a Backbone.Model, that model will try to find instances of relations using the repository.

Throughout this readme, we will use the following example:

```
var MUser = Backbone.Model.extend();
var MAuthor = Backbone.Model.extend({
    relations: {
        user: MUser
    }
});
var MEditor = Backbone.Model.extend();
var CWriter = Backbone.Collection.extend();
var MPost = Backbone.Model.extend({
    relations: {
        author: MAuthor,
        editor: MEditor,
        writers: CWriter
    },
});
```


# Setting a model

## Scalar

The default `Backbone.Model.set` does not give a clear meaning when setting a scalar value (int, string, null). Here we override this behavior: if you use `set` with a scalar, it is interpreted as setting the id of that model:

```
mAuthor = new MAuthor(5);
mAuthor.get('id'); // -> 5

mAuthor.set(7);
mAuthor.get('id'); // -> 7
```

If given a repository and a matching id can be found, then the attributes of that model will be copied:

```
cRepository = new Backbone.Collection([{{
    id: 'author',
    collection: new Backbone.Collection([{id: 7, name: 'Burhan'}])
}}]);

mAuthor = new MAuthor(5, {repository: cRepository, main: 'author'});
mAuthor.get('id');      // -> 5
mAuthor.get('name');    // -> ''

mAuthor.set(7);
mAuthor.get('id');      // -> 7
mAuthor.get('name');    // -> 'Burhan'
```


## Backbone.Model

You can also give a `Backbone.Model` to `set`. It will then copy the given `Backbone.Model`'s attributes to itself, essentially making a clone.

```
mAuthor = new MAuthor(5);
mAuthor.get('id');      // -> 5

mAuthor.set(new Backbone.Model(7));
mAuthor.get('id');      // -> 7
```


# Setting a collection

## Array of ids

If you set an array of numbers to a vanilla Backbone.Collection, it will create empty models. Here we change that behaviour so that models with those ids will be created:

```
mAuthor = new MAuthor({writers: [1, 2, 3]});
mAuthor.get('writers').pluck('id');     // -> [1, 2, 3]

mAuthor.set('writers', [5, 6, 7]);
mAuthor.get('writers').pluck('id');     // -> [5, 6, 7]
```

If given a repository and a matching id can be found, then the attributes of that model will be copied:

```
cRepository = new Backbone.Collection([{{
    id: 'writers',
    collection: new Backbone.Collection([
        {id: 1, name: 'foo'},
        {id: 2, name: 'bar'},
        {id: 3, name: 'baz'},
    ])
}}]);

mAuthor = new MAuthor({writers: [1, 2, 3], {repository: cRepository, main: 'author'});
mAuthor.get('writers').pluck('name'); // -> ['foo', 'bar', 'baz']
```
