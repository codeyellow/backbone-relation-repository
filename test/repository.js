/* global Backbone */
/* global BackboneModel */
/* global QUnit */
(function () {
    const HashMap = function (data) {
        const that = this;

        this.byId = {};
        _.each(data, (item) => {
            that.byId[item.id] = item;
        });

        this.get = function (id) {
            return this.byId[id];
        };
    };

    QUnit.module('backbone-relation-repository should ');
    const MUser = Backbone.Model.extend();
    const MAuthor = Backbone.Model.extend({
        relations: {
            user: MUser,
        },
    });
    const MEditor = Backbone.Model.extend();
    const CWriter = Backbone.Collection.extend();
    const MPost = Backbone.Model.extend({
        relations: {
            author: MAuthor,
            editor: MEditor,
            writers: CWriter,
        },
    });

    QUnit.test('be', 1, (assert) => {
        const mAuthor = new MAuthor();

        assert.ok(mAuthor instanceof Backbone.Model, 5, 'an instance of backbone-relation.');
    });

    QUnit.test('set the id', 1, (assert) => {
        const mAuthor = new MAuthor();
        mAuthor.set(5);

        assert.equal(mAuthor.get('id'), 5, 'if an int is given.');
    });

    QUnit.test('copy attributes', 2, (assert) => {
        const mAuthor = new MAuthor();
        const model = new Backbone.Model({ id: 6, name: 'Burhan' });

        mAuthor.set(model);

        assert.ok(mAuthor.get('id') === 6 && mAuthor.get('name') === 'Burhan', 'if a Backbone.Model is given.');
        assert.ok(mAuthor.attributes !== model.attributes, 'but cannot reference the same attributes object.');
    });

    QUnit.test('retrieve from repository', 3, (assert) => {
        const meuk2 = {
            repository: new Backbone.Collection([
                { id: 'author', collection: new Backbone.Collection([{ id: 5, name: 'Burhan', user: 2 }]) },
                { id: 'user', collection: new Backbone.Collection([{ id: 2, name: 'Zainuddin' }]) },
                { id: 'writers', collection: new Backbone.Collection([{ id: 1, name: 'foo' }, { id: 2, name: 'bar' }, { id: 3, name: 'baz' }]) },
            ]),
        };

        const mPost = new MPost({}, meuk2);

        mPost.set('author', 5);
        mPost.set('writers', [1, 2, 3]);
        assert.ok(mPost.dot('author.id') === 5 && mPost.dot('author.name') === 'Burhan', 'if a known id is given.');
        assert.ok(mPost.dot('author.user.id') === 2 && mPost.dot('author.user.name') === 'Zainuddin', 'should work with nested relations.');
        assert.deepEqual(mPost.dot('writers').pluck('name'), ['foo', 'bar', 'baz'], 'should work with related collections.');
    });

    QUnit.test('setting a related collection', 2, (assert) => {
        const mPost = new MPost({ writers: [1, 2, 3] });

        assert.deepEqual(mPost.get('writers').pluck('id'), [1, 2, 3], 'if an array of int is given, it should create models with those ids.');

        mPost.set('writers', [5, 6, 7]);
        assert.deepEqual(mPost.get('writers').pluck('id'), [5, 6, 7], 'if an array of int is set, it should create models with those ids.');
    });

    QUnit.test('support join_attributes', 2, (assert) => {
        const MPostJoinAttributes = MPost.extend({
            relations: {
                author: {
                    relationClass: MAuthor,
                    join_attributes: ['join'],
                },
            },
        });
        const mPostJoinAttributes = new MPostJoinAttributes();
        const options = {
            repository: new Backbone.Collection([
                { id: 'author', collection: new HashMap([{ id: 5, name: 'Burhan', user: 2 }]) },
            ]),
        };

        mPostJoinAttributes.set({ author: { id: 5, join: 'Obi Wan' } }, options);

        assert.equal(mPostJoinAttributes.dot('author.name'), 'Burhan');
        assert.equal(mPostJoinAttributes.dot('author.join'), 'Obi Wan');

        // deepEqual(mPost.get('writers').pluck('id'), [1, 2 ,3], 'if an array of int is given, it should create models with those ids.');

        // mPost.set('writers', [5, 6, 7]);
        // deepEqual(mPost.get('writers').pluck('id'), [5, 6, 7], 'if an array of int is set, it should create models with those ids.');
    });

    QUnit.test('support join_attributes with existing value', 4, (assert) => {
        const MPostJoinAttributes = MPost.extend({
            relations: {
                author: {
                    relationClass: MAuthor,
                    join_attributes: ['join'],
                },
                writers: {
                    relationClass: CWriter,
                    join_attributes: ['other'],
                },
            },
        });
        const mPostJoinAttributes = new MPostJoinAttributes();
        const options = {
            repository: new Backbone.Collection([
                { id: 'author', collection: new HashMap([{ id: 5, name: 'Burhan', user: 2, join: 'should be overwritten' }]) },
                { id: 'writers', collection: new HashMap([{ id: 8, name: 'Zainuddin', user: 4, other: 'another thing' }]) },
            ]),
        };

        mPostJoinAttributes.set({
            author: { id: 5, join: 'Obi Wan' },
            writers: [{ id: 8, other: 'R2D2' }],
        }, options);

        assert.equal(mPostJoinAttributes.dot('author.name'), 'Burhan');
        assert.equal(mPostJoinAttributes.dot('author.join'), 'Obi Wan');
        assert.equal(mPostJoinAttributes.get('writers').get(8).get('name'), 'Zainuddin');
        assert.equal(mPostJoinAttributes.get('writers').get(8).get('other'), 'R2D2');
    });

    QUnit.test('compatibility with Backbone', 3, (assert) => {
        const mUser = new MUser();

        assert.ok(mUser instanceof Backbone.Model);
        assert.ok(mUser instanceof BackboneModel);
        assert.ok(new Backbone.Collection()._isModel(mUser));
    });

    QUnit.skip('should be able to define direct recursive relations', 2, (assert) => {
        let MAuthorWithPost;
        const MPostWithAuthor = Backbone.Model.extend({
            type: 'post',
            relations() {
                return {
                    author: MAuthorWithPost,
                };
            },
        });
        MAuthorWithPost = Backbone.Model.extend({
            type: 'author',
            relations() {
                return {
                    post: MPostWithAuthor,
                };
            },
        });

        const mPost = new MPostWithAuthor(null, { createRelations: false });

        const options = {
            repository: new Backbone.Collection([
                { id: 'author', collection: new HashMap([{ id: 5, name: 'Burhan' }]) },
                { id: 'post', collection: new HashMap([{ id: 8, title: 'hoi', author: 5 }]) },
            ]),
        };

        mPost.set({
            author: 5,
        }, options);

        assert.equal(mPost.dot('author.name'), 'Burhan');
        assert.equal(mPost.dot('author.post'), undefined);
    });

    QUnit.skip('should be able to indirect recursive relations', 1, (assert) => {
        let CCampaign;
        let MClient;

        const MProduct = Backbone.Model.extend({
            relations() {
                return {
                    client: MClient,
                };
            },
        });

        const CProduct = Backbone.Collection.extend({
            model: MProduct,
        });

        MClient = Backbone.Model.extend({
            relations() {
                return {
                    campaigns: CCampaign,
                };
            },
        });

        const MCampaign = Backbone.Model.extend({
            relations() {
                return {
                    client: MClient,
                    products: CProduct,
                };
            },
        });

        CCampaign = Backbone.Collection.extend({
            model: MCampaign,
        });

        const mProduct = new MProduct(null, { createRelations: false });
        const options = {
            repository: new Backbone.Collection([
                { id: 'client', collection: new HashMap([{ id: 10, name: 'Burhan', products: [1], campaigns: [5] }]) },
                { id: 'campaigns', collection: new HashMap([{ id: 5, title: 'hoi', products: [1] }]) },
            ]),
        };

        mProduct.set({
            id: 1,
            campaigns: [5],
            client: 10,
        }, options);

        // assert.equal(mPost.dot('author.name'), 'Burhan');
        // assert.equal(mPost.dot('author.post'), undefined);
        assert.equal(1, 1);
    });


  // test('setting a related model', 5, function() {
  //   var MPostProxy = MPost.extend({createRelations: false});
  //   var mPost = new MPost();
  //   var authorAttributes = {id: 5, name: 'Burhan Zainuddin'};
  //   var mAuthor = new MAuthor({id: 6, name: 'AB Zainuddin'});

  //   mPost.set('author', 5);
  //   equal(mPost.get('author').get('id'), 5, 'with a scalar will set the id.');

  //   mPost.set('author', authorAttributes);
  //   deepEqual(mPost.get('author').toJSON(), authorAttributes, 'with a hash will set the related model.');

  //   mPost.set('author', mAuthor);
  //   deepEqual(mPost.get('author').toJSON(), mAuthor.toJSON(), 'with a model will copy the attributes to the related model.');
  //   ok(mPost.get('author') !== mAuthor, 'with a model will not overwrite the related model.');

  //   mPost = new MPostProxy();
  //   mPost.set('author', null);
  //   ok(mPost.get('author') instanceof MAuthor, 'will create that relation if not created before.');
  // });

  // test('setting a related collection', 1, function() {
  //   var mPost = new MPost();
  //   var cWriter = new CWriter([
  //       {id: 5, name: 'Burhan Zainuddin'},
  //       {id: 6, name: 'AB Zainuddin'}
  //   ]);
  //   var cWriterOthers = new CWriter([
  //       {id: 7, name: 'Burhan Zainuddin'},
  //       {id: 8, name: 'AB Zainuddin'}
  //   ]);

  //   debugger;

  //   mPost.get('writers').set(cWriter);
  //   mPost.get('writers').set(cWriterOthers);
  //   console.log(mPost.get('writers').pluck('id'));
  //   equal(cWriter.length, 4, 'with a collection will append the given models.');
  // });
}());
