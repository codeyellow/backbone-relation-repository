import path from 'path';

export default {
    entry: path.join(__dirname, 'index.js'),
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'backbone-relation-repository.js',
        library: 'backbone-relation-repository',
        libraryTarget: 'umd',
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel',
            },
        ],
    },
    externals: {
        backbone: {
            root: 'Backbone',
            commonjs: 'backbone',
            commonjs2: 'backbone',
            amd: 'backbone',
        },
        'backbone-relation': {
            root: 'backbone-relation',
            commonjs: 'backbone-relation',
            commonjs2: 'backbone-relation',
            amd: 'backbone-relation`',
        },
        underscore: {
            root: '_',
            commonjs: 'underscore',
            commonjs2: 'underscore',
            amd: 'underscore',
        },
    },
};
