# Installing dependencies

Running

```
npm run deps
```

will install the Backbone submodule and install npm packages. From this point on you can run

```
npm test
```
