## 0.0.6
- Added support for `relations` being a function.
- Moved `backbone` and `backbone-relation` to `peerDependencies`.
 
## 0.0.5
- Fixed join_attributes bug. If a join_attribute was already defined in the HashMap (`{id: 1, join: 'this', name: 'Burhan'}`), it would not overwrite it with the given join_attribute (`{id: 1, join: 'that'}`).

## 0.0.1
- Add join_attributes support and HashMap idea.
